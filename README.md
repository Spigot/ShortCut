# Shortcut

A Minecraft plugin for creating command shortcuts without needing another plugin

> *Reload ready! This plugin has no adverse effects when /reload is executed: commands will be disabled and added according to the changes to the configuration*

## Configuration

Each command can be individually configured

The basic yaml structure is as follows:

```yml
# This is the command name/label that you will type ingame (think /command_name)
command_name:
  # An OPTIONAL permission to use, this can be anything
  # If the sender doesn't have this permission they cannot run this shortcut
  permision: example.permission
  # If the command can only be executed by players (this is also optional) 
  # For none players the execution method returns instantly
  playerOnly: false
  # An OPTIONAL way to describe how the command should be used
  usage: "/command_name <value>"
  # What is actually used to control what the shortcut does
  executions:
    - {
        # This is the command to run
        # %%0 is replaced with the first argument provided, %%1 is the second and so on
        # Placeholders can be used if you so choose
        run: say %%0,
        # Who to run the command as
        # Must be either "sender" OR "console"
        as: sender
    }
    - {
        # This is a message that is to be sent to the user
        # Formatting and placeholders can be used here
        display: Example message with §aFormatting!
    }

```

### Example

The following is an example for a "gmc" command shortcut:

> Placeholders are utilised in this example, ensure you have PlaceHolderApi if you wish to use it

```yml
# the command label to use
gmc:
  # permission required to run this shortcut
  # this doesn't bypass the check on the original command if proxying!
  # placeholders do NOT work here
  permission: command.gamemode
  # if the command can only be ran by a player
  # placeholders do NOT work here
  playerOnly: true
  # displayed when an invalid version of this command is issued
  # placeholders do NOT work here
  usage: /gmc
  executions:
    # runs the command "gamemode creative" as the sender
    # placeholders can be used here
    - { run: gamemode creative %player_name%, as: console }
    # displays this message to the executor
    # placeholders can be used here
    - { display: "§0[§a✓§0]§r You are now in creative mode" }
```

Below is each element explained:

| Component | Used Value | Description |
| --- | --- | --- |
| Permission | command.gamemode | Specifies the permission required to run the "gmc" command. Users attempting to execute this command must have the "command.gamemode" permission. |
| Player-Only | true | When set to `true`, this configuration ensures that only players (as opposed to console or other entities) can execute the "gmc" command. |
| Usage | /gmc | The "usage" field specifies the command syntax that users should follow to invoke the "gmc" command. |

Execution Step 1

| Action | Command to Run | As Sender |
| --- | --- | --- |
| Run a command | `gamemode creative %player_name%` | `console` |

This step instructs the system to set the executor's game mode to creative. The `%player_name%` placeholder is replaced with the executor's name. The command is executed as the console.

Execution Step 2

| Action | Message to Display |
| --- | --- |
| Display a message | "§0[§a✓§0]§r You are now in creative mode" |

This step displays a message to the executor indicating that they are now in creative mode. The message includes formatting for visual appeal.