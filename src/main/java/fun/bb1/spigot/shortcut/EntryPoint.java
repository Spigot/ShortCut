package fun.bb1.spigot.shortcut;

import java.util.List;

import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.Nullable;

import fun.bb1.spigot.shortcut.config.Config;

/**
 *    Copyright 2023 BradBot_1
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
public final class EntryPoint extends JavaPlugin {
	
	private @Nullable List<Shortcut> shortcuts;
	
	@Override
	public void onLoad() {
		this.saveDefaultConfig();
		super.onLoad();
	}
	
	@Override
	public void onEnable() {
		this.getLogger().info("Thank you for using shortcut!");
		if (this.shortcuts != null) {
			this.getLogger().warning("Commands being reregistered without disabling them! This will cause issues");
		}
		this.shortcuts = new Config(this).getData().stream().map(Shortcut::new).toList();
		this.shortcuts.forEach(shortcut -> {
			shortcut.register();
			this.getLogger().info("Registered the shortcut \"" + shortcut.getLabel() + '"');
		});
	}
	
	@Override
	public void onDisable() {
		if (this.shortcuts != null) {
			this.shortcuts.forEach(Shortcut::unregister);
			this.shortcuts = null;
		}
	}
	
}
