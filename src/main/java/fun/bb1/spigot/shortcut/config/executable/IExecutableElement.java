package fun.bb1.spigot.shortcut.config.executable;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.NotNull;

/**
 *    Copyright 2023 BradBot_1
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
@FunctionalInterface
public interface IExecutableElement extends CommandExecutor {
	
	@Internal
	public static @NotNull IExecutableElement forSect(@NotNull final Map<?, ?> section) {
		@NotNull final Map<String, Object> newMap = new HashMap<String, Object>();
		section.entrySet().forEach(e -> newMap.put(e.getKey().toString(), e.getValue()));
		if (section.containsKey("run")) return new RunExecutable(newMap);
		if (section.containsKey("display")) return new DisplayExecutable(newMap);
		return (sender, alias, arguments) -> {};
	}
	
	public void executeFor(@NotNull final CommandSender sender, @NotNull final String aliasUsed, @NotNull final String[] arguments);
	
	@Override
	default boolean onCommand(CommandSender arg0, Command arg1, String arg2, String[] arg3) {
		this.executeFor(arg0, arg2, arg3);
		return true;
	}
	
}
