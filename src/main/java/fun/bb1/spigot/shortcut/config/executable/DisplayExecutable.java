package fun.bb1.spigot.shortcut.config.executable;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.NotNull;

import me.clip.placeholderapi.PlaceholderAPI;

/**
 *    Copyright 2023 BradBot_1
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
@Internal
final class DisplayExecutable implements IExecutableElement {
	
	private static final @NotNull String MESSAGE_KEY = "display";
	
	private final @NotNull String message;
	
	@Internal
	DisplayExecutable(@NotNull final Map<String, Object> section) {
		this.message = section.getOrDefault(MESSAGE_KEY, "Nothing to say, maybe configure this?").toString();
	}
	
	@Override
	public final void executeFor(@NotNull final CommandSender sender, @NotNull final String aliasUsed, @NotNull final String[] arguments) {
		sender.sendMessage((sender instanceof final Player player && Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) ? PlaceholderAPI.setPlaceholders(player, this.message) : this.message);
	}

}
