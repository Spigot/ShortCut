package fun.bb1.spigot.shortcut.config;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import fun.bb1.spigot.shortcut.config.executable.IExecutableElement;

/**
 *    Copyright 2023 BradBot_1
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
public final record ShortcutData(@NotNull String alias, @Nullable String usage, @Nullable String permission, boolean playerOnly, @NotNull IExecutableElement[] executions) {
	
	public final @NotNull String getUsage() {
		if (this.usage != null) return this.usage;
		return "/" + alias;
	}
	
	public final boolean canBeExecutedBy(@NotNull final CommandSender sender) {
		if (this.playerOnly && !(sender instanceof Player)) return false;
		if (this.permission == null) return true;
		return sender.hasPermission(this.permission);
	}
	
	public final void executeFor(@NotNull final CommandSender sender, @NotNull final String aliasUsed, @NotNull final String[] arguments) {
		for (final IExecutableElement execution : this.executions) {
			execution.executeFor(sender, aliasUsed, arguments);
		}
	}
	
}
