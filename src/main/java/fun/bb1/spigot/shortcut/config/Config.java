package fun.bb1.spigot.shortcut.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.NotNull;

import fun.bb1.spigot.shortcut.config.executable.IExecutableElement;

/**
 *    Copyright 2023 BradBot_1
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
@Internal
public final class Config {
	
	private final @NotNull Plugin plugin;	
	
	@Internal
	public Config(final @NotNull Plugin plugin) {
		this.plugin = plugin;
	}
	
	public final @NotNull List<ShortcutData> getData() {
		final List<ShortcutData> datas = new ArrayList<ShortcutData>();
		for (final String key : this.plugin.getConfig().getKeys(false)) {
			final ConfigurationSection commandSection = this.plugin.getConfig().getConfigurationSection(key);
			final List<IExecutableElement> executables = new ArrayList<IExecutableElement>();
			if (commandSection.contains("executions")) {
				for (final Map<?, ?> executionMap : commandSection.getMapList("executions")) {
					if (executionMap.isEmpty()) continue;
					executables.add(IExecutableElement.forSect(executionMap));
				}
			}
			datas.add(new ShortcutData(key, commandSection.getString("usage"), commandSection.getString("permission"), commandSection.getBoolean("playerOnly", false), executables.toArray(IExecutableElement[]::new)));
		}
		return datas;
	}
	
}
