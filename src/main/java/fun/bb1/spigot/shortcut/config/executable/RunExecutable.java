package fun.bb1.spigot.shortcut.config.executable;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.NotNull;

import me.clip.placeholderapi.PlaceholderAPI;

/**
 *    Copyright 2023 BradBot_1
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
@Internal
final class RunExecutable implements IExecutableElement {
	
	private static final @NotNull String COMMAND_KEY = "run";
	private static final @NotNull String SENDER_KEY = "as";
	private static final @NotNull String SUBSTITUTION = "%%";
	
	private final @NotNull String command;
	private final boolean performSubstitution;
	private final @NotNull ExecuteAs executeAs;
	
	@Internal
	RunExecutable(@NotNull final Map<String, Object> section) {
		this.command = section.getOrDefault(COMMAND_KEY, "say You need to provide a command to run!").toString();
		this.performSubstitution = this.command.contains(SUBSTITUTION);
		this.executeAs = section.getOrDefault(SENDER_KEY, "sender").toString().toLowerCase().equals("console") ? ExecuteAs.CONSOLE : ExecuteAs.SENDER;
	}
	
	@Override
	public final void executeFor(@NotNull final CommandSender sender, @NotNull final String aliasUsed, @NotNull final String[] arguments) {
		String toRun = this.command;
		if (this.performSubstitution) {
			for (int i = 0; i < arguments.length; i++) {
				toRun = toRun.replaceAll(SUBSTITUTION + i, arguments[i]);
			}
		}
		if (sender instanceof final Player player && Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
			toRun = PlaceholderAPI.setPlaceholders(player, toRun);
		}
		Bukkit.dispatchCommand(this.executeAs == ExecuteAs.CONSOLE ? Bukkit.getConsoleSender() : sender, toRun);
	}
	
	private static enum ExecuteAs {
		SENDER,
		CONSOLE,
		;
	}

}
