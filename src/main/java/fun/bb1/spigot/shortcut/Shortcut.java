package fun.bb1.spigot.shortcut;

import java.lang.reflect.Field;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.jetbrains.annotations.ApiStatus.Internal;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import fun.bb1.spigot.shortcut.config.ShortcutData;

/**
 *    Copyright 2023 BradBot_1
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
@Internal
final class Shortcut extends BukkitCommand {
	
	private @NotNull ShortcutData data;
	private boolean disabled = false;

	@Internal
	Shortcut(@NotNull final ShortcutData data) {
		super(data.alias());
		this.update(data);
	}

	@Override
	public final boolean execute(final @NotNull CommandSender arg0, final @NotNull String arg1, final @NotNull String[] arg2) {
		if (this.disabled) return false;
		if (!this.data.canBeExecutedBy(arg0)) return false;
		this.data.executeFor(arg0, arg1, arg2);
		return true;
	}
	
	@Override
	public final @NotNull List<String> tabComplete(final @NotNull CommandSender sender, final @NotNull String alias, final @NotNull String[] args) throws IllegalArgumentException {
		if (this.disabled) return List.of();
		return List.of(); // maybe?
	}
	
	@Internal
	final void register() {
		try {
			Field bukkitCommandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            bukkitCommandMap.setAccessible(true);
            CommandMap commandMap = (CommandMap) bukkitCommandMap.get(Bukkit.getServer());
            @Nullable Command cmd = commandMap.getCommand(this.data.alias());
            if (cmd != null && cmd instanceof final Shortcut shortcut) {
            	shortcut.update(this.data);
            	this.disabled = false;
            	return;
            }
            cmd = commandMap.getCommand("shortcut:" + this.data.alias());
            if (cmd != null && cmd instanceof final Shortcut shortcut) {
            	shortcut.update(this.data);
            	this.disabled = false;
            	return;
            }
            commandMap.register("shortcut", this);
            this.disabled = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Internal
	final void update(@NotNull final ShortcutData data) {
		this.data = data;
		this.setLabel(this.data.alias());
		if (this.data.permission() != null) this.setPermission(this.data.permission());
		this.setUsage(this.data.getUsage());
	}
	
	@Internal
	final void unregister() {
		this.setPermission(UUID.randomUUID().toString().replace('-', '.')); // make inaccessible
		this.disabled = true;
	}

}
